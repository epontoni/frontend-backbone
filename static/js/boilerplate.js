define([
	// path alias que configuramos en el main.js
	'jquery',		// vendor/jquery-1.11.1.min
	'underscore',	// vendor/underscore-1.7.0.min
	'backbone'		// vendor/backbone-1.1.2.min
	], function($, _, Backbone){
		// Pasamos jQuery, Underscore y Backbone
		// Ellos no serán accesibles en el scope global
		return {};
		// Lo que retornamos aqui será usado por otros módulos.
	});