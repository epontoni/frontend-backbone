require.config({
    paths: {
        csrf_ajax:  'vendor/csrf_ajax',
        jquery:     ['http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min', 'vendor/jquery-1.10.2.min'],
        underscore: ['http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min', 'vendor/underscore-min'],
        backbone:   ['http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min', 'vendor/backbone-min']
                    // Quitar http: en production
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ["underscore", "jquery"],
            exports: 'Backbone'
        },
        csrf_ajax: {
            deps: ["jquery"],
            exports: 'csrf_ajax'
        }
    }
});

require([
    // Cargamos el módulo "app" y lo pasamos a nuestra función de definición
    'csrf_ajax',
    'app'
], function(csrf_ajax, App){
    
    console.log('[*] main.js');

    // La dependencia (módulo) "app" (archivo app.js) es pasada (lo recibimos) como "App", que retorna un objeto App
    window.App = App; // var App = App.App;
    window.App.initialize();
});